import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SessionService } from '../session/session.service';
import { tap, map, catchError } from  'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SurveyService {

  constructor(private http: HttpClient, private session: SessionService) { }

  // With RxJS
  getSurveys(): Promise<any> {
    return this.http.get( `${environment.apiUrl}/v1/api/surveys`)
    .pipe(
      tap(response => {
        console.log(response);
       }),

       map((response: any) => response.data || []),

       catchError(error => {
         if(error.status === 404) {
           return throwError('Could not connect to Survey puppy. Please try again later.');

         }
          return throwError(error)
       })
    )
    .toPromise();
  }
  // Old solution before RxJS
  // getSurveys(): Promise<any> {
  //   return this.http.get( `${environment.apiUrl}/v1/api/surveys`).toPromise();
  // }

  getSurveyById(surveyId): Promise<any> {
    return this.http.get( `${environment.apiUrl}/v1/api/surveys/${surveyId}` ).toPromise();
  }

}
